# Enviromentals

### To create a new enviromental "env\_name"

```
python -m venv evn_name
```

To active the new enviroments, from terminal:

```
evn_name\Scripts\activate
```

### How to Delete the Environment

In case that you want to remove the environment, you can simply run:

```
rm -rf env_name
```

### Create a requirement.txt file

Using the `pip freeze` command we can generate the `requirement.txt` file based on the libraries that we installed in our virtual environment.

In the terminal of the activated virtual environment, we can run:

```
pip freeze > requirements.txt
```

