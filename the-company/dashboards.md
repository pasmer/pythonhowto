---
description: using Streamlit libray
cover: >-
  https://images.unsplash.com/photo-1552664730-d307ca884978?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2970&q=80
coverY: 0
---

# Dashboards

## How to run Streamlite

{% hint style="info" %}
**Path error:** attention to the path name.
{% endhint %}

Run on terminal

```
python -m streamlit run [APP filename]
```

